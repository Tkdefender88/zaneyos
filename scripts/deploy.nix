{pkgs, ...}:
pkgs.writeShellScriptBin "deploy" ''
  #!/usr/bin/env bash

  set -e

  if [ $# -eq 0 ]
    then echo "usage: deploy <branch>" && exit 1
  fi

  target=$1
  branch=$(git rev-parse --abbrev-ref HEAD)

  git checkout master && git pull

  git checkout $target

  git reset --hard origin/master

  git merge --squash $branch

  git commit -m "deploy $branch to $target"

  git push -f

  git checkout $branch
''

