local keymap = vim.keymap
-- use jk to exit insert mode
keymap.set("i", "jk", "<ESC>", { desc = "Exit insert mode with jk" })
-- clear search highlights
keymap.set("n", "<leader>nh", ":nohl<CR>", { desc = "Clear search highlights" })
-- window management
keymap.set("n", "<leader>sv", "<C-w>v", { desc = "Split window vertically" }) -- split window vertically
keymap.set("n", "<leader>sh", "<C-w>s", { desc = "Split window horizontally" }) -- split window horizontally
keymap.set("n", "<leader>se", "<C-w>=", { desc = "Make splits equal size" }) -- make split windows equal width & height
keymap.set("n", "<leader>sx", "<cmd>close<CR>", { desc = "Close current split" }) -- close current split window
keymap.set("n", "<leader>to", "<cmd>tabnew<CR>", { desc = "Open new tab" }) -- open new tab
keymap.set("n", "<leader>tx", "<cmd>tabclose<CR>", { desc = "Close current tab" }) -- close current tab
keymap.set("n", "<leader>tn", "<cmd>tabn<CR>", { desc = "Go to next tab" }) --  go to next tab
keymap.set("n", "<leader>tp", "<cmd>tabp<CR>", { desc = "Go to previous tab" }) --  go to previous tab
keymap.set("n", "<leader>tf", "<cmd>tabnew %<CR>", { desc = "Open current buffer in new tab" }) --  move current buffer to new tab

keymap.set("n", "<leader>fm", "<CMD>lua vim.lsp.buf.format()<CR>", { desc = "format the buffer"})
keymap.set("n", "Y", "yg$", { desc = "yank to the end of the line"})
keymap.set("n", "J", "mzJ`z", { desc = "move the line up"})
keymap.set("n", "<C-d>", "<C-d>zz", { desc = "" })
keymap.set("n", "<C-u>", "<C-u>zz", { desc = "" })
keymap.set("n", "n", "nzzzv", {desc = "move to the next instance and center in the view"})
keymap.set("n", "N", "Nzzzv", {desc = "move to the prev instance and center in the view"})
keymap.set("n", "<leader>y", "\"+Y", { desc = "yank to the system clipboard"})
keymap.set("n", "<leader>dv", ":Ex<CR>", {desc = "open the file view"})
keymap.set("n", "<Tab>", ":bnext<CR>", {desc = "next buffer"})
keymap.set("n", "<S-Tab>", ":bprev<CR>", {desc = "prev buffer"})
