{config, pkgs, ... }:

{
    programs.tmux = {
        enable = true;
        keyMode = "vi";
        terminal = "tmux-256color";
        historyLimit = 10000;

        extraConfig = with config.theme; with pkgs.tmuxPlugins;
        ''
            set-option -g status-justify left
            set-option -g status-position top
            set-option -g status-left-length 200
            set-option -g status-right-length 200

            set-option -g status-fg green
            set-option -g status-bg black
        '';
    };
}
