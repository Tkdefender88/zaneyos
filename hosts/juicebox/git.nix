
{...}:
let
    inherit(import ./variables.nix ) gitUsername gitEmail;
in{
      # Install & Configure Git
    programs.git = {
        enable = true;
        userName = "${gitUsername}";
        userEmail = "${gitEmail}";
        aliases = {
            ci = "commit";
            co = "checkout";
            sup = "status";
            br = "branch";
            yeet = "push";
            yoink = "pull";
            yank = "fetch";
        };

        extraConfig = {
            push = { autoSetupRemote = true; };
        };
    };
}
