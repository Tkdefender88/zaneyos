{
  # Git Configuration ( For Pulling Software Repos )
  gitUsername = "Justin Bak";
  gitEmail = "me@justinbak.com";

  # Base16 Theme
  theme = "dracula";

  # Hyprland Settings
  borderAnim = true; # Enable / Disable Hyprland Border Animation
  extraMonitorSettings = "";

  # Waybar Settings
  clock24h = true;
  waybarAnimations = false;

  # Program Options
  browser = "firefox"; # Set Default Browser (google-chrome-stable for google-chrome)
  terminal = "st"; # Set Default System Terminal
}
